#!/usr/bin/env node 

const fs = require('fs');
const util = require('util');
const chalk = require('chalk');
const path = require('path');

//Method #2
// const lstat = util.promisify(fs.lstat);

//Method #3
const lstat = fs.promises.lstat;
//or: const { lstat } = fs.promises;

const targetDir = process.argv[2] || process.cwd();

console.log(process.argv);

fs.readdir(targetDir, async (err, filenames) => {
    if (err){
        //error handling code here 
        console.log(err);
    }
    const statPromises = filenames.map(filename => {
        // return lstat(filename);
        return lstat(path.join(targetDir, filename));
    })

    const allStats = await Promise.all(statPromises);

    for (let stats of allStats){
        const index = allStats.indexOf(stats);

        if(stats.isFile()){
            console.log(filenames[index]);
        }
        else {
            console.log(chalk.bold(filenames[index]));
        }
        // console.log(filenames[index], stats.isFile());
    }

    //This code goes with 3 methods:
    // for (let filename of filenames){
    //     try{
    //         const stats = await lstat(filename);

    //         console.log(filename, stats.isFile());
    //     }
    //     catch (err){
    //         console.log(err);
    //     } 
    // }

    //Bad Code:
    // for (let filename of filenames){
    //     fs.lstat(filename, (err, stats) => {
    //         if(err) {
    //             console.log(err);
    //         }
    //         console.log(filename, stats.isFile());
    //     });
    // };
    //Bad code complete

    //Fixing bad code:
    // const allStats = Array(filenames.length).fill(null);
    // for (let filename of filenames){
    //     const index = filenames.indexOf(filename);
    //     fs.lstat(filename, (err, stats) => {
    //         if(err) {
    //             console.log(err);
    //         }
    //         allStats[index] = stats;

    //         const ready = allStats.every(stats => {
    //             return stats;
    //         });

    //         if (ready){
    //             allStats.forEach((stats, index) => {
    //                 console.log(filename[index], stats.isFile());
    //             });
    //         }
    //     });
    // };


});

//Method #1
// const lstat = (filename) => {
//     return new Promise((resolve, reject) => {
//         fs.lstat(filename, (err, stats) => {
//             if(err){
//                 reject(err);
//             }
//             resolve(stats);

//         })
//     })
// }


//install chalk packages: console log with colors 
//npm install chalk